#ifdef _MSC_VER
#  define NOINLINE __declspec(noinline)
#else
#  define NOINLINE __attribute__((noinline))
#endif

#include <vector>


// The add_vectors_simple is less efficient in terms of memory usage. Reason for this is 
// that a large vector is loaded from memory in order to perform the operation (i.e. loop
// over the elements). 
// For the add_vectors_singleloop, the piece of data to be loaded from the memory when 
// executing the code is much smaller (equal to the number of vectors) and hence much faster.
// The takehome lesson is thus that it is better/faster to load often a small amount of data, than a 
// few times a huge amount of data.

// Specialisation for one parameter
template<typename V>
static double add_elems(int i, const std::vector<V> &head)
{
    return head[i];
};

// Generic version
template<typename V, typename ... T>
static double add_elems(const int i, const std::vector<V> &head, const T&... tail ){
    return head[i]+add_elems(i,tail ... );   
};  


// TODO: Replace the following function template.  You may change the template
// arguments and function arguments if necessary.
template<typename V, typename... T>
std::vector<V> add_vectors_singleloop(
    const std::vector<V>& head, const T&... tail)
{
    std::vector<V> result(head.size());
    int length = head.size();
    for ( int i = 0; i<length; i++){
        result[i] = add_elems(i,head,tail ... );
    }
    
//     std::vector<V> result(head.size());
    return result;
}


template<typename V>
std::vector<V> add_vectors_simple(const std::vector<V> &head, const std::vector<V> &tail ){
    std::vector<V> result(head.size());
    for (auto i=0; i<head.size(); i++){
        result[i] = head[i]+tail[i];
    }
    return result;
};

template<typename V, typename ... T>
std::vector<V> add_vectors_simple(
    const std::vector<V>& head, const T&... tail)
{
//     std::vector<V> result(head.size());
    return add_vectors_simple(head, add_vectors_simple(tail ... ));
//     return result;
}




NOINLINE std::vector<double> test_add_vectors_singleloop(
    const std::vector<double>& a, const std::vector<double>& b,
    const std::vector<double>& c, const std::vector<double>& d)
{
    return add_vectors_singleloop(a, b, c, d);
}

NOINLINE std::vector<double> test_add_vectors_simple(
    const std::vector<double>& a, const std::vector<double>& b,
    const std::vector<double>& c, const std::vector<double>& d)
{
    return add_vectors_simple(a, b, c, d);
}

#include <iostream>
#include <cstring>

int main(int argc, char **argv)
{
    int n = 1000000;
//     int n = 10;
    std::vector<double> a(n);
    std::vector<double> b(n);
    std::vector<double> c(n);
    std::vector<double> d(n);
    std::vector<double> e(n);
//     // Testing only
//     a[1] = 100;
//     b[1]= 200;
//     c[1]=300;
//     e = add_vectors_singleloop(a,b,c);
//     double f;
//     f = add_elems((int)1,a,b,c);
//     std::cout<<f<<std::endl;
//     for (int i = 0; i<e.size(); i++ ){
//         std::cout<<e[i]<<std::endl;    
//     }
    
    if (argc == 2)
    {
        if (strcmp(argv[1], "simple") == 0)
        {
            std::cout << "testing simple" << std::endl;
            for (int i = 0; i < 100; i++)
                test_add_vectors_simple(a, b, c, d);
            return 0;
        }
        else if (strcmp(argv[1], "singleloop") == 0)
        {
            std::cout << "testing singleloop" << std::endl;
            for (int i = 0; i < 100; i++)
                test_add_vectors_singleloop(a, b, c, d);
            return 0;
        }
    }
    std::cout << "USAGE: " << argv[0] << " simple|singleloop" << std::endl;
}
