/**
 * \file swap.cxx
 *
 * This file is part of the course wi4771tu:
 * Object Oriented Scientific Programming in C++
 *
 */

// Include header file for standard input/output stream library
#include <iostream>

void value_swap(int i1, int i2){
   int i3 = i1;
   int i4 = i2;

   i1 = i4;
   i2 = i3;
}

void address_swap(int* p1, int* p2){
   int p3; 
   int p4;
   
   p3 = *p1;
   p4 = *p2;
   *p1 = p4;
   *p2 = p3;	
}

void reference_swap(int& i1, int& i2){
   int i3 = i1;
   int i4 = i2;
   
   i1 = i4;
   i2 = i3;
}

// The global main function that is the designated start of the program
int main(){

    // Read two integer values
    std::cout<<"Insert two integers"<<std::endl; 
    int i1; std::cin >> i1;
    int i2; std::cin >> i2;

    // Make two copies of i1 and i2
    int i3 = i1;
    int i4 = i2;
    int i5 = i1;
    int i6 = i2;

    // Value swap
    value_swap(i1,i2);
    std::cout<<"Swap by value returns i1 = "<<i1<<" i2 = "<<i2<<std::endl; 
    
    // Reference swap
    int* p1 = &i3;
    int* p2 = &i4;
    address_swap(p1, p2);
    std::cout<<"Swap by address returns i1 = "<<*p1<<" i2 = "<<*p2<<std::endl; 
    
    // Address swap
    reference_swap(i5,i6);
    std::cout<<"Swap by reference returns i1 = "<<i5<<" i2 = "<<i6<<std::endl;
    // Return code 0 to the operating system (=no error)
    return 0;
}

