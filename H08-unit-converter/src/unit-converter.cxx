/**
 * 
 * 
 * \file unit-converter.cxx
 * 
 * \author Jakob Maljaars
 * 
 */

#include<iostream>
#include<typeinfo>

//  Make the enum class
enum class Unit{ km , m , cm};

// Measure trait
template<int v, Unit u>
struct Measure
{
   static const int length = v;
   static const Unit unit = u;
};

// Make an empty _Measure_add trait
template<int v1, Unit u1, int v2, Unit u2>
struct _Measure_add;

// Make the 7 unque combinations
template<int v1, Unit u1, int v2>
struct _Measure_add<v1, u1, v2, u1>
{
    static const int length = v1 +v2 ;
    static const Unit unit = u1;
};

template<int v1, int v2>
struct _Measure_add<v1,Unit::km, v2, Unit::m>
{
    static const int length = 1000*v1 +v2 ;
    static const Unit unit = Unit::m; 
};

template<int v1, int v2>
struct _Measure_add<v1,Unit::km, v2, Unit::cm>
{
    static const int length = 1000*100*v1 +v2 ;
    static const Unit unit = Unit::cm; 
};

template<int v1, int v2>
struct _Measure_add<v1,Unit::m, v2, Unit::cm>
{
    static const int length = 100*v1 +v2 ;
    static const Unit unit = Unit::cm; 
};

template<int v1, int v2>
struct _Measure_add<v1,Unit::cm, v2, Unit::m>
{
    static const int length = v1 + 100*v2 ;
    static const Unit unit = Unit::cm; 
};

template<int v1, int v2>
struct _Measure_add<v1,Unit::cm, v2, Unit::km>
{
    static const int length = v1 + 100*1000*v2 ;
    static const Unit unit = Unit::cm; 
};

template<int v1, int v2>
struct _Measure_add<v1,Unit::m, v2, Unit::km>
{
    static const int length = v1 + 1000*v2 ;
    static const Unit unit = Unit::m; 
};

// The generic Measure_add trait
template<typename A, typename B >
struct Measure_add
{    
   static const int length = _Measure_add<A::length, A::unit, B::length, B::unit>::length;
   static const Unit unit = _Measure_add<A::length, A::unit, B::length, B::unit >::unit; 
};

int main()
{
    std::cout << Measure_add<Measure<10,Unit::km>, Measure<20, Unit::m>>::length<<std::endl;
    std::cout << Measure_add<Measure<10,Unit::km>, Measure<2000, Unit::cm>>::length<<std::endl;
    std::cout << Measure_add<Measure<10,Unit::m>, Measure<20, Unit::m>>::length<<std::endl;
    
    return 0;
};


