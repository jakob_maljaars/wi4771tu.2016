/*

    Comment on `const` and `&` here.  See ../README.md.
    The rule for using const is relatively simple: use it wherever
    and whenever possible. This avoids unwanted behavior. For example
    if it is given that a method does not alter the object, make sure 
    the object remains constant (eg. for the + and - operator)
    
    Objects are alwasy passed by reference (i.e. they are marked by &)

 */

// Support for precompiled header files (only under MS Visual Studio)
#ifdef _MSC_VER
#include "stdafx.h"
#endif

#include <cmath>
#include <ostream>
#include <iomanip>

class Point
{
public:
    
    // constructors
    Point()
    { 
	x = 0.;
	y = 0.;
    }
    Point(const double x, const double y)
    {
        this->x = x;
        this->y = y;
    }
    // operators
    Point operator+(const Point& other)const
    {   
	Point pn(x+other.x,y+other.y);
        return pn;
    }
    
    Point operator-(const Point& other)const
    {   
	Point pn(x-other.x,y-other.y);
        return pn;
    }

    Point& operator+=(const Point& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }
    
    Point& operator-=(const Point& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }
    // methods
    double distance()
    {
	double d = sqrt(pow(this->x,2)+pow(this->y,2));
	return d;
    }
    
    double distance(Point& other)
    {
	Point diff = *this-other;
        double d = diff.distance();
        return d;
    }
    
    Point rotated(const double angle)const
    {
	Point pn(x*cos(angle)-y*sin(angle),
		 x*sin(angle)+y*cos(angle));
	return pn;
    }

    Point rotated(const double angle, const Point& other)const
    {
	Point diff = *this - other;
        Point pnt = diff.rotated(angle);
// 	Point pnt(diff.x*cos(angle)-diff.y*sin(angle),
// 		 diff.x*sin(angle)+diff.y*cos(angle));
//	Point pn = other+pnt;
	return other+pnt;
    }

    Point& rotate(const double angle)
    {
// 	double xt = x;
// 	double yt = y;	
// 	this->x= xt*cos(angle)-yt*sin(angle);
// 	this->y= xt*sin(angle)+yt*cos(angle);
        
        *this = this->rotated(angle);
	return *this;
    }
    
    Point& rotate(const double angle, const Point& other)
    {
        *this = this->rotated(angle,other);
        return *this;
    }
    double x;
    double y;
};

class Triangle
{
public:
    // constructors
    Triangle()
    {
        Point a(0,0);   Point b(0,0);   Point c(0,0);  
    }
    
    // Delegating is not really helpful here, but anyway:
    Triangle(const Point& a, const Point& b , const Point& c) : Triangle()
    {   
        this->a = a;
        this->b = b;
        this->c = c;
    }
    
    // operators
    // methods
    Triangle translated(const Point& t)const
    {
        Point d = a+t;
        Point e = b+t;
        Point f = c+t;
        Triangle v(d,e,f);
        return v;
    }
    
    Triangle& translate(const Point&t)
    {
        *this = this->translated(t);
        return *this;
    }
    
    
   Triangle rotated(const double angle)const
    {
        Point d = a.rotated(angle);
        Point e = b.rotated(angle); 
        Point f = c.rotated(angle);
        Triangle v(d,e,f);
        return v;
    }

    Triangle rotated(const double angle, const Point& other)const
    {
        Point diffa = a-other;
        Point diffb = b-other;
        Point diffc = c-other;
        Triangle difft(diffa, diffb, diffc);
        Triangle rotdiff = difft.rotated(angle).translate(other);
        return rotdiff;
    }
    
    Triangle& rotate(const double angle)
    {
        *this = this->rotated(angle);
        return *this;
    }
    
    Triangle& rotate(const double angle, const Point& other)
    {
        *this = this->rotated(angle, other);
        return *this;
    }
    
    double volume()
    {   
        double volume = 0.5*(a.x*b.y+b.x*c.y+c.x*a.y
                            -b.x*a.y-c.x*b.y-a.x*c.y);
        return volume; 
    }
    
    Point a, b, c;
};

// ostream operator for `Point`s
std::ostream &operator<<(std::ostream &os, const Point &p)
{
    // remember current flags, precision
    auto flags = os.flags();
    auto current_precision = os.precision();
    // output numers with fixed point and three decimal precision
    os.setf(std::ios::fixed, std::ios::floatfield);
    os.precision(3);
    // output point `p`
    os << "(" << std::setw(6) << std::setfill(' ') << p.x
        << "," << std::setw(6) << std::setfill(' ') << p.y << ")";
    // restore current flags, precision
    os.flags(flags);
    os.precision(current_precision);
    return os;
}

// ostream operator for `Triangle`s
std::ostream &operator<<(std::ostream &os, const Triangle &t)
{
    return os << "Triangle< " << t.a << ", " << t.b << ", " << t.c <<  " >";
}


#include <iostream>
using namespace std;

int main()
{
    const double pi = M_PI;
    // Scope for Point tests
    {
        Point p(1,1);
        Point q(2,2);
        
        // Test + and - operator
        Point r = p+q;
        Point s = p-q;
        std::cout << "Result point addition: " << r << std::endl;
        std::cout << "Result point subtraction: " << s << std::endl;
        
        // Test in-place operators += and -= 
        p+=q;
        std::cout << "Result in-place addition: " << p << std::endl;
        p-=q;
        std::cout << "Result in-place subtraction: " << p << std::endl;
        
        // Test distance methods
        std::cout << "Distance q to origin: " << q.distance() << std::endl;
        std::cout << "Distance q to p: " << q.distance(p) << std::endl;
        
        // Test rotate
        Point k = p.rotated(pi/2.);
        Point l = p.rotated(pi/2.,q);
        std::cout << "Rotate p around origin: " << k << std::endl;
        std::cout << "Rotate p around q: " << l << std::endl;
        
        // Test rotate in-place
        p.rotate(pi);
        std::cout << "Rotate p in-place around origin: " << p << std::endl;
        q.rotate(pi);
        std::cout << "Rotate q in-place around origin: " << q << std::endl;
    }
    
    // Scope for triangle tests
    { 
    std::cout << " " << std::endl;
    Point p(1,1);
    Point q(5.25,8.244);
    Point r(1,2);
    Point z(2.5,-1.5);
    
    Triangle t(p,q,r);
    std::cout << "Triangle t " << t << std::endl;
    std::cout << "Volume t " << t.volume() << std::endl;
    // Test translate and translated
    Triangle u = t.translated(p);
    std::cout << "Translated triangle " << u << std::endl;
    t.translate(p);
    std::cout << "Translate triangle " << t << std::endl;
    
    // Test rotate
    Triangle v = t.rotated(pi/2);
    std::cout << "Rotated triangle " << v << std::endl;
    Triangle w = t.rotated(pi/2,p);
    std::cout << "Rotated triangle " << w << std::endl;
    
    // Test in place rotation
    t.rotate(pi/2);
    std::cout << "Rotate triangle " << t << std::endl;
    t.rotate(pi/2,p);
    std::cout << "Rotate triangle " << t << std::endl;
    
    }
// 
//     // set point `p` to (1, 2)
//     p.x = 1;
//     p.y = 2;
//     Point q(3,4);
//     Point a=p+p;
//     Point c = p-q;
//     p+=q;
// //    std::cout<<a
//     // set triangle `t` to (1, 2), (3, 4), (5, 6)
// //    t.a.x = 1;
// //    t.a.y = 2;
// //    t.b.x = 3;
// //    t.b.y = 4;
// //    t.c.x = 5;
// //    t.c.y = 6;
// 
//     // print point `p` and triangle `t`
//     std::cout << p << std::endl;
//     std::cout << a << std::endl;
//     std::cout << c << std::endl;
// //    cout << t << endl;
}
