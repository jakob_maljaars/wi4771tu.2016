/**
 * \file arrays.cxx
 *
 * This file is part of the course wi4771tu:
 * Object Oriented Scientific Programming in C++
 *
 */

// Include header file for standard input/output stream library
#include <iostream>
#include <stdexcept>

int* add_arrays(const int* array1, const int* array2, int n){
     int* array3 = new int[n];
     for(int i=0; i<n; i++){
         // No dereferenceing required for array1 and array2
         array3[i] = array1[i]+array2[i];
     }	
     return array3;
}


// The global main function that is the designated start of the program
int main(){

    // Insert the size of the arrays
    std::cout<<"Size of array 1: "<<std::endl;
    int n1; std::cin >> n1; 
    std::cout<<"Size of array 2: "<<std::endl;
    int n2; std::cin >> n2;
    if ( n1 == 0 || n2 == 0 ){
       throw std::invalid_argument("I cannot deal with unequal array size");
    }else if (n1 != n2){
       throw std::invalid_argument("I cannot deal with unequal array size");
    }

    // Initialize arrays (with equal size)
    int array1[n1];
    int array2[n1];
    
    // Now we start to insert the values
    std::cout<<"Insert values for array 1"<<std::endl;
    for(int i=0; i<n1; i++){
       std::cin >> array1[i];	
    }
    std::cout<<"Insert values for array 2"<<std::endl;
    for(int i=0; i<n1; i++){
       std::cin >> array2[i];	
    } 
    // Call the add_arrays function
    int* array3 = add_arrays(array1,array2,n1);
    // Now print the result
    std::cout<<"The result of the array adding is printed"<<std::endl;
    for(int i=0; i<n1; i++){
       std::cout<<*array3++<<std::endl;
    }
    return 0;
}
