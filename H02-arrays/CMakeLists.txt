# Force CMake version 2.6 or above
cmake_minimum_required (VERSION 2.6)

# This project has the name: H02-arrays
project (H02-arrays)

# Create an executable named 'arrays' from the source file 'arrays.cxx'
add_executable(arrays src/arrays.cxx)