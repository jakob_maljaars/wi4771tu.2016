/**
 * \file numerical-differentiation.cxx
 * 
 * \author Jakob Maljaars
 * 
 */

#include <iostream>
#include <cmath>
#include <functional>

class Derivative
{
//     protected:
    private:
    double h_default = 1e-8;
    
    public:
    double h;
//     public: 
    // Constructors
    Derivative ()
    {
        h = h_default;
        std::cout<< "Default derivative constructor called"<<std::endl;
    }
    
    explicit Derivative( double h ):
    h(h)
    {
        std::cout<< "Double h derivative constructor called"<<std::endl;
    }
    
    //Destructor 
    ~Derivative() 
    {
        h = h_default;
        std::cout<< "Derivative destructor called"<<std::endl;
    }
    
    
    
    // Methods
    // This virtual function appears to work for both function pointers
    // as well as lambda expressions
    virtual double differentiate(
        std::function<double(double)> func, double x) = 0;    
    
};

class CentralDifference: public Derivative
{
    public: 
        // Inherit the constructors from Base class (why are the constructors
        // not inherited automatically?)
        CentralDifference(): Derivative(){}
        explicit CentralDifference(double h): Derivative(h){}
        
        // Methods
        virtual double differentiate(
            std::function<double(double)> func, double x)
        {
            return (func(x+h)-func(x-h))/(2*h);
        }
};

class ForwardDifference: public Derivative
{
    public:
     // Inherit constructors from BaseClass
     ForwardDifference(): Derivative(){}
     explicit ForwardDifference(double h): Derivative(h){}
     
     // Methods
     virtual double differentiate(
     std::function<double(double)> func, double x)
     {
        return (func(x+h)-func(x))/h;
     }
};


// Classical function
const double myfunc1(const double x) { return sin(2*M_PI*x); }

// Lambda expression for function 2
auto myfunc2 = [](double x){return  pow(x,2)+x;};


int main()
{   
    
    double h= 1e-1;
    
    // Central difference, using customized Derivative constructor
    CentralDifference CD1; 
    std::cout << ""<<std::endl;
    std::cout << "=== Output for default central difference ==="<<std::endl;
    std::cout << "Default increment h = "<<CD1.h<<std::endl;
    std::cout << "Central difference for myfunc = sin(2*pi*x) at x= 1 (period); classical function: " 
    << CD1.differentiate(myfunc1, (double)1) << std::endl;
    std::cout << "Central difference for myfunc = x²+x at x = 10; lambda expression used:  " 
    << CD1.differentiate(myfunc2, (double)10) << std::endl;
    std::cout << "============================================="<<std::endl;   
    std::cout << ""<<std::endl;
    // Central difference, using customized Derivative constructor
    CentralDifference CD2(h);
    std::cout << "=== Output for customized central difference ==="<<std::endl;
    std::cout << "Customized increment h = "<<CD2.h<<std::endl;
    std::cout << "Central difference for myfunc = sin(2*pi*x) at x= 1 (period); classical function: " 
    << CD2.differentiate(myfunc1, (double)1) << std::endl;
    std::cout << "Central difference for myfunc = x²+x at x = 10; lambda expression used:  " 
    << CD2.differentiate(myfunc2, (double)10) << std::endl;
    std::cout << "============================================="<<std::endl;   
    std::cout << ""<<std::endl;
    
    // Forward Difference (not Four Wheel Drive :P ), using default Derivative constructor
    ForwardDifference FWD1;
        std::cout << "=== Output for default forward difference ==="<<std::endl;
    std::cout << "Default increment h = "<<FWD1.h<<std::endl;
    std::cout << "Forward difference for myfunc = sin(2*pi*x) at x= 1 (period); classical function: " 
    << FWD1.differentiate(myfunc1, (double)1) << std::endl;
    std::cout << "Forward difference for myfunc = x²+x at x = 10; lambda expression used:  " 
    << FWD1.differentiate(myfunc2, (double)10) << std::endl;
    std::cout << "============================================="<<std::endl; 
    std::cout << ""<<std::endl;
    // And with customized Derivative constructor
    ForwardDifference FWD2(h);    
    std::cout << "=== Output for customized forward difference ==="<<std::endl;
    std::cout << "Customized increment h = "<<FWD2.h<<std::endl;
    std::cout << "Forward difference for myfunc = sin(2*pi*x) at x= 1 (period); classical function: " 
    << FWD2.differentiate(myfunc1, (double)1) << std::endl;
    std::cout << "Forward difference for myfunc = x²+x at x = 10; lambda expression used:  " 
    << FWD2.differentiate(myfunc2, (double)10) << std::endl;
    std::cout << "============================================="<<std::endl; 
    std::cout << ""<<std::endl;
    return 0;
}