/**
 * \file symbolic-differentiation.cxx
 * 
 * \author Jakob Maljaars
 * 
 *
 */


#include<iostream>
#include<cmath>

struct Sin;
struct Cos;

template<int value>
struct ConstInt{
    static const double eval(double x)
    {
        return value;
    };
    typedef ConstInt<0> deriv; 
};

template<typename A, typename B>
struct Add{
    static const double eval(double x)
    {
        return A::eval(x)+B::eval(x);
    };
    
    typedef Add<typename A::deriv,typename B::deriv> deriv;
};

template<typename A, typename B>
struct Mul{
    static const double eval(double x)
    {
        return A::eval(x)*B::eval(x);
    };
    
    typedef Add<Mul<typename A::deriv, B>,  Mul<A, typename B::deriv>> deriv;
};

template<int exponent>
struct Monomial{
    static const double eval(double x)
    {
        return pow(x,exponent);
    };
    typedef Mul<ConstInt<exponent>, Monomial<exponent-1>> deriv;
};


template<typename A>
struct Neg{
    static const double eval(double x)
    {
        return -1.*A::eval(x);
    };
    typedef Neg<typename A::deriv> deriv;     
    
    
};

struct Sin{
    static const double eval(double x)
    {
        return sin(x);
    };
    typedef Cos deriv; 
};

struct Cos{
    static const double eval(double x)
    {
        return cos(x);
    };
    typedef Neg<Sin> deriv;     
};



int main(){
    // Test the symbolic differentiation functionality
    std::cout<<"Output for symbolic differentiation test"<<std::endl;
    std::cout<<"A prime denotes a derivative!"<<std::endl;
    std::cout<<"Function value for y(x) = 10 at x = 5 equals "<<ConstInt<10>::eval((double) 5)<<std::endl;
    std::cout<<"Function value for y'(x) = 10 at x = 5 equals "<<ConstInt<10>::deriv::eval((double) 5)<<std::endl;
    std::cout<<"Function value for y(x) = x^3 at x = 5 equals "<<Monomial<3>::eval((double) 5)<<std::endl;
    std::cout<<"Function value for y'(x) = x^3 at x = 5 equals "<<Monomial<3>::deriv::eval((double) 5)<<std::endl;
    
    std::cout<<"Function value for y(x) = 4*x^3 at x = 5 equals "<<Mul<ConstInt<4>,Monomial<3>>::eval((double) 5)<<std::endl;
    std::cout<<"Function value for y'(x) = 4*x^3 at x = 5 equals "<<Mul<ConstInt<4>,Monomial<3>>::deriv::eval((double) 5)<<std::endl;
    
    std::cout<<"Function value for y(x) = x^3+x^2 at x = 5 equals "<< Add<Monomial<3>,Monomial<2>>::eval((double) 5) <<std::endl;
    std::cout<<"Function value for y'(x) = x^3+x^2 at x = 5 equals "<< Add<Monomial<3>,Monomial<2>>::deriv::eval((double) 5) <<std::endl;
    
    std::cout<<"Function value for y(x) = x^3+4*x^2 at x = 5 equals "<<Add<Monomial<3>,Mul<ConstInt<4>,Monomial<2>>>::eval((double) 5)<<std::endl;
    std::cout<<"Function value for y'(x) = x^3+4*x^2 at x = 5 equals "<<Add<Monomial<3>,Mul<ConstInt<4>,Monomial<2>>>::deriv::eval((double) 5)<<std::endl;
    
    std::cout<<"Function value for y(x) = sin(x) at pi/2 equals "<<Sin::eval((double) M_PI/2)<<std::endl;
    std::cout<<"Function value for y'(x) = sin(x) at pi/2 equals "<<Sin::deriv::eval((double) M_PI/2)<<std::endl;
    std::cout<<"Function value for y\"(x) = sin(x) at pi/2 equals "<<Sin::deriv::deriv::eval((double) M_PI/2)<<std::endl;
    
    return 0;
}