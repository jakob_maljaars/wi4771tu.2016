# Force CMake version 3.1 or above
cmake_minimum_required (VERSION 3.1)

# This project has the name: H09-symbolic-differentiation
project (H09-symbolic-differentiation)

# Create an executable named 'symbolic-differentiation'
add_executable(symbolic-differentiation src/symbolic-differentiation.cxx)
target_compile_features(symbolic-differentiation PRIVATE cxx_auto_type)
target_compile_features(symbolic-differentiation PRIVATE cxx_uniform_initialization)