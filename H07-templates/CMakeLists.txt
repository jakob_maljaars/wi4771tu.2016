# Force CMake version 3.1 or above
cmake_minimum_required (VERSION 3.1)

# This project has the name: H07-templates
project (H07-templates)

# Create an executable named 'templates' from the source file 'templates.cxx'
add_executable(templates src/templates.cxx)
target_compile_features(templates PRIVATE cxx_auto_type)
target_compile_features(templates PRIVATE cxx_uniform_initialization)