/**
 * \file templates.cxx
 * 
 * \author Jakob Maljaars
 * 
 */

#include<iostream>
#include<typeinfo>
#define PRINT_EXPRESSION(expr) std::cout << #expr << ": " << (expr) \
            << " (type: " << typeid(expr).name() << ")" << std::endl


// Since the add_simple function takes two arguments of 
// similar type, the returntype will be the same as the 
// arguments
template<typename A>
A add_simple(A a, A b)
{
    return a+b;
}

template<typename A, typename B>
auto add(A a, B b) -> decltype(a+b)
{
    return a+b;
}

template<typename A>
bool is_int(A a)
{
    return false;
}

template<>
bool is_int<int>(int a)
{
    return true;
}

template<typename T>
class Number
{
    public:
        const T value;
    
    Number(T value): value(value)
    {
        std::cout<<"Constructor called"<<std::endl;
    }
    
    template<typename U>
    auto operator+(const Number<U>& other) -> Number<decltype(this->value+other.value)>
    { return this->value+other.value;}
    
    template<typename U>
    auto operator-(const Number<U>& other) -> Number<decltype(this->value-other.value)>
    { return this->value-other.value;}
    
    template<typename U>
    auto operator*(const Number<U>& other) -> Number<decltype(this->value*other.value)>
    { return this->value*other.value;}
    
    template<typename U>
    auto operator/(const Number<U>& other) -> Number<decltype(this->value/other.value)>
    { return this->value/other.value;}
};

template<long long int N>
struct fibonacci
{
    
    static const long long int value = fibonacci<N-1>::value+fibonacci<N-2>::value;
};

template<>
struct fibonacci<1>{
    static const long long int value =0;
};

template<>
struct fibonacci<2>{
    static const long long int value =1;
};

int main()
{
    double a = 1.5;
    double b = 1.75;
    std::cout<< "Test add_simple with input of type double"<< std::endl; 
    PRINT_EXPRESSION(add_simple(a,b));
    std::cout<< " "<<std::endl;
    std::cout<< "Test add with input of type int and type double"<< std::endl; 
    int c = 1;
    PRINT_EXPRESSION(add(a,c));
    std::cout<< " "<<std::endl;
    std::cout<< "Test is_int with input of type double"<< std::endl; 
    PRINT_EXPRESSION(is_int(a));
    std::cout<< "Test is_int with input of type int"<< std::endl; 
    PRINT_EXPRESSION(is_int(c));
    std::cout<< " "<<std::endl;
    
    Number<double> N1(3.5);
    Number<int> N2(3);
    auto N3 = N1+N2;
    std::cout<< "The value of the double+int operation is: "<<N3.value << std::endl; 
    std::cout<< "The return type of the double+int operation is: "<<typeid(N3.value).name() << std::endl; 
    
    auto N4 = N1-N2;
    std::cout<< "The value of the double-int operation is: "<<N4.value << std::endl; 
    std::cout<< "The return type of the double-int operation is: "<<typeid(N4.value).name() << std::endl; 
    
    auto N5 = N1*N2;
    std::cout<< "The value of the double*int operation is: "<<N5.value << std::endl; 
    std::cout<< "The return type of the double*int operation is: "<<typeid(N5.value).name() << std::endl; 
    
    auto N6 = N1/N2;
    std::cout<< "The value of the double/int operation is: "<<N6.value << std::endl; 
    std::cout<< "The return type of the double/int operation is: "<<typeid(N6.value).name() << std::endl; 
    std::cout<< " "<< std::endl;
    
    const int j = 5;
    std::cout<< "The "<< j << " Fibonacci number equals " << fibonacci<j>::value<<std::endl;
    
    const int i = 80;
    std::cout<< "The "<< i << " Fibonacci number equals " << fibonacci<i>::value<<std::endl;
    return 0;
}

