/**
 * \file copy-move.cxx
 *
 * This file is part of the course wi4771tu:
 * Object Oriented Scientific Programming in C++
 *
 * \author Jakob Maljaars
 *
 */

// Include relevant header files
#include <iostream>
#include <initializer_list>
#include <memory>

class Container {
    private:
        double* data;
        int length;    
    public: 
    // Constructors
    Container (){
        length = 0;
        data = nullptr; 
        print("Constructor ( <empty> ) called");
    }
/*    explicit Container(int length){
        this->length = length;
        this->data = new double[length];   
        print("Constructor (int length) called");
    }  */  

    //Same as outcommented constructor, but now using delegation
    explicit Container(int length): 
        length(length),
        data(new double[length])
    {  
        print("Constructor (int length) called");
    }
    
    explicit Container(std::initializer_list<double> array): 
        Container( (int)array.size() )            // Delegate the Container( int length ) constructor
    {
        std::uninitialized_copy(array.begin(), array.end(), data);
        print("Uninitialized copy constructor called");
    }  
    
    // Deep-copy constructor
    explicit Container(const Container& c):
    Container(c.length)
    {
        for (auto i=0; i<c.length; i++)
            data[i] = c.data[i];
        std::clog << "Copy constructor" << std::endl;
    }
    
    // Move constructor
    Container(Container&& c)
    : length(c.length), data(c.data)
    {
        c.length = 0;
        c.data = nullptr;
        std::clog << "Move constructor" << std::endl;
    }
       
    // Destructor
    ~Container()
    {
        length = 0;
        delete[] data;
        print("Destructor called");
    }
    
    
    // Methods
    void print(const std::string &info) const
    {
        // print the address of this instance, the attributes `length` and
        // `data` and the `info` string
        std::cout << "  " << this << " " << length << " " << data << "  "
            << info << std::endl;
    }  
    
   
    // Copy assignment
    Container& operator=(const Container& other)
    {
        if (this != &other)
            {
                delete[] data;
                length = other.length;
                data   = new double(other.length);
                for (auto i=0; i<other.length; i++)
                    data[i] = other.data[i];
            }
        std::clog << "Copy assignment operator" << std::endl;
        return *this;
    }

    // Move assignment
    Container& operator=(Container&& other)
    {
        if (this != &other)
            {
                delete[] data;
                length = other.length;
                data   = other.data;
                other.length = 0;
                other.data   = nullptr;
            }
        std::clog << "Move assignment operator" << std::endl;
        return *this;
    }
    
    Container operator+(const Container& other)
    {   
        if( this->length == other.length )
        {       
            Container pn(other.length);
            for (auto i=0; i<other.length; i++)
            {
                pn.data[i] = this->data[i]+other.data[i]; 
            }
/*                delete[] data;
            length = other.length;
            data   = new double(other.length);
            for (auto i=0; i<other.length; i++)
                data[i] = other.data[i];   */     
            return pn;
        }        
        
    }
};

int main()
    {
        // Make Container a using the implicit "uninitialized copy constructor"
        // This constructor will call the (int length) constructor (by delegation)
        std::cout << "Container a({ 1, 2, 3 });" << std::endl;
        Container a({ 1, 2, 3 });                               
        std::cout << "  a has address " << &a << std::endl;
        
        // Create a container b = { ... } means an implicit declaration
        // However, the constructors were of explicit type, therefore the 
        // implicit declaration b = {... } won't work. Hence, use again an 
        // explicit declaration
        std::cout << "Container b = { 4, 5, 6 };" << std::endl;
        Container b({ 4, 5, 6 });
        std::cout << "  b has address " << &b << std::endl;
        
        // Deep-copy Container a to Container c.
        // Hence address of the data pointer as well as 
        // the data pointer for c must be different from that of 
        // Container a
        std::cout << "Container c(a);" << std::endl;
        Container c(a);
        std::cout << "  c has address " << &c << std::endl;
        
        // Container d is constructed as follow:
        // first the rhs is evaluated resulting in a new Container.
        // Given the move assignment operater, Container is subsequently
        // MOVED to Container d
        std::cout << "Container d = a + b;" << std::endl;
        Container d = a + b;
        std::cout << "  d has address " << &d << std::endl;
        
        // Initialize an empty Container e
        std::cout << "Container e;" << std::endl;
        Container e;
        std::cout << "  e has address " << &e << std::endl;
        // Move the result of the operation a+b to Container e
        std::cout << "e = a + b;" << std::endl;
        e = a + b;
        
        // Move the result of a+b to Container f 
        std::cout << "Container f(std::move(a + b));" << std::endl;
        Container f(std::move(a + b));
        std::cout << "  f has address " << &f << std::endl;
        
        // This really is a nested operation. So first a+b ->d is computed
        // and finally d+c is computed. Given the move assignment implementation
        // temporal containers are moved and destroyed to the head so that a 
        // nested operation won't consume more than a 'one step' implementation
        
        // I must admit that I don't really see the difference between the implementation
        // for g, h and i (given the explicit constructor declaration). 
        // Also, the end result (in terms of numeric values) is fortunately the same. Data 
        // can be accessed from main if data attribute is set to public.
        std::cout << "Container g = a + b + c;" << std::endl;
        Container g = a + b + c;
        std::cout << "  g has address " << &g << std::endl;
        
        std::cout << "Container h;" << std::endl;
        Container h;
        std::cout << "  h has address " << &h << std::endl;
        std::cout << "h = a + b + c;" << std::endl;
        h = a + b + c;
        std::cout << "  h has address " << &h << std::endl;
        
        std::cout << "Container i = { a + b + c };" << std::endl;
        Container i = { a + b + c };
        std::cout << "  i has address " << &i << std::endl;
        std::cout << "end" << std::endl;

        return 0;
}